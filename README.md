# DLCM Jupyter Notebooks

This repository showcases potential use cases of the [official DLCM Python Library](https://pypi.org/project/dlcm-api/). Tutorials notebook have been written with detailed explanations and hands-on examples on how to use the python library to interact with a [DLCM backend](https://gitlab.unige.ch/dlcm/community/dlcm-backend). 

### Notebook: DLCM-MakeADeposit.ipynb
This notebook explains how the python library can be used for:
1. the retrieval of basic metadata relevant to an archival deposit, such as Organizational Units, Licenses, Languages and Contributors. 
2. the creation and upload of a deposit.
3. make automatized multiple deposits from a CSV file leveraging the two previous points.