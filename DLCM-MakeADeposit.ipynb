{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "1d566082-9af5-487f-ae8c-fcb74f922e1c",
   "metadata": {},
   "source": [
    "## Make a single deposit on DLCM\n",
    "\n",
    "This notebook showcases how to create a deposit on a DLCM instance (such as Yareta or OLOS) using the official [DLCM API Python package](https://gitlab.unige.ch/dlcm/community/dlcm-python-api) to interact with a DLCM server.\n",
    "\n",
    "\n",
    "You can install it using the command indicated at the top of the [package's pypi page](https://pypi.org/project/dlcm-api/). If you're using a jupyterhub instance administrated by the UNIGE, run the cell below, if no error message is launched, that means the package is already installed."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "ab50530a-91cd-4dc4-b74e-2fad47d71b8b",
   "metadata": {},
   "outputs": [],
   "source": [
    "import dlcmapi_client"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "317a6f86-667b-4a66-a57e-34bf03158931",
   "metadata": {},
   "source": [
    "A deposit on DLCM requires several pre-existing information at minimum before being created: \n",
    " * Its `Organizational Unit ID` that has defaults\n",
    "     * `Submission Policy ID` \n",
    "     * `Preservation Policy ID` \n",
    " * One or several `Contributors ID`\n",
    "\n",
    "And although not mandatory, it is also recommended to prepare your deposit with the following fields as well:\n",
    " * A `License ID` \n",
    " * A `Language ID` \n",
    "\n",
    "Those are all identifiers of pre-existing entities that need to be retrieved from your DLCM's instance server, hence before dwelling in the actual Deposit creation process, we are going to show you how to use the DLCM python client in order to fetch all those IDs.\n",
    "\n",
    "### Preparation: retrieve your access token\n",
    "\n",
    "Some interactions we are going to make with DLCM's backend require authentication, which is possible with the python client using an _access token_. \n",
    "\n",
    "To retrieve this token, go to your DLCM instance's home page, log in to your account, and then click on your account icon on the top right of the page. Select the \"Token\" submenu.\n",
    "\n",
    "![dlcm-token-select](imgs/dlcm_token_select.png \"Select your token\")\n",
    "\n",
    "In the resulting pop-up click on the \"Copy to clipboard\" button to retrieve the token's value.\n",
    "\n",
    "![dlcm-token-copy](imgs/dlcm_copy_token.png \"Copy sour token\")\n",
    "\n",
    "Then run the cell below and once prompted, paste it in the form and hit the ENTER key."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "84022790-7590-46cd-93e6-505a0326c651",
   "metadata": {},
   "outputs": [],
   "source": [
    "import getpass\n",
    "my_access_token = getpass.getpass(\"User token?\") "
   ]
  },
  {
   "cell_type": "markdown",
   "id": "32950a4d-8aa4-4a7b-853f-446ef8108130",
   "metadata": {},
   "source": [
    "Your token is now saved in the variable `my_access_token`, as we are going to use this token several times through the notebook. Note that this token is only valid for 24 hours, you'll need to go back to your user profile if you need to interact with the backend through the `dlcmapi_client` library after that time span."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "0a6ef378-c1bb-4af2-9926-808830b68909",
   "metadata": {},
   "source": [
    "### Finding your Organizational Unit Id\n",
    "\n",
    "The first required ID relates to the Organizational Unit within which the data has to be deposited. The python library provides a function called `admin_authorized_organizational_units_get` which returns a collection of all the organizational units your user account has access to. This is an administration function, meaning you need to target the `Admin` module URL of your DLCM's instance. The list of DLCM modules' URLs can be accessed through the \"about\" button at the bottom right corner of your DLCM's instance home page:\n",
    "\n",
    "![dlcm-about-button](imgs/dlcm_about_button.png \"DLCM About Button\")\n",
    "\n",
    "Loading the following pages shows all the different modules, identify the one called admin and copy the URL **up until the last slash**:\n",
    "\n",
    "![dlcm-modules-list](imgs/dlcm_admin_modules_list.png \"DLCM modules you can target\")\n",
    "\n",
    "Combined with the access token, this is sufficient to instantiate a `dlcmapi_client.Configuration` object that will be useful to interact with the administration backend of DLCM:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "3096f6f4-11cf-4a5e-acf3-e693a0c5d6d7",
   "metadata": {},
   "outputs": [],
   "source": [
    "import dlcmapi_client\n",
    "admin_url = 'https://sandbox.dlcm.ch/administration'\n",
    "admin_conf = dlcmapi_client.Configuration(admin_url)\n",
    "admin_conf.access_token = my_access_token"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "c448adbf-c051-473e-b9f0-1955f3de826f",
   "metadata": {},
   "source": [
    "This configuration object is used to instantiate an ApiClient object, which in turns is what can help us produce and AdminApi object that is used to call the function `admin_authorized_organizational_units_get` :"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "50c48fd6-0261-42e7-8a9e-00c9b0b65f54",
   "metadata": {},
   "outputs": [],
   "source": [
    "with dlcmapi_client.ApiClient(admin_conf) as api_client:\n",
    "    admin_api = dlcmapi_client.AdminApi(api_client)\n",
    "    admin_data = admin_api.admin_authorized_organizational_units_get(size=20)\n",
    "    \n",
    "admin_data.to_dict()['data'][:2] #showing only the first two values to avoid the notebook to become too cluttered."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "960071e3-96e8-48e2-85a6-9b7358e2c0a1",
   "metadata": {},
   "source": [
    "As can be seen, the collection that is returned is quite voluminous and contains various fields relating to organizational units (Such as their default submission and preservation policies ID). In the cell below, we wrote a small python routine down below that will list all your organizational unit by name and ask you to select it. The routine will save the identifiers of the Organizational Unit, and its default preservation and submission policies in the three variables at the bottom of the cell:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "a7aeb468-6d9c-4f6c-8cd8-317864aeab7d",
   "metadata": {},
   "outputs": [],
   "source": [
    "data = admin_data.to_dict()['data']\n",
    "\n",
    "print('\\033[1mList of your Organizational Units:\\033[0m')\n",
    "for i, elem in enumerate(data):\n",
    "    print(f'{\"%2.2d\"%i} -> {elem[\"name\"]}')\n",
    "print()\n",
    "\n",
    "sel = input('Which organizational unit would you like to deposit data in (enter its index number located on the left of the arrows)?')\n",
    "while not sel.isnumeric() or int(sel) < 0 or int(sel) >= len(data):\n",
    "    sel = input(f'Please enter a positive integer below {len(data)}')\n",
    "    \n",
    "data_selected = data[int(sel)]\n",
    "\n",
    "#saving orgunit, preservation and submission policies identifiers in dedicated variables\n",
    "orgunit_id, orgunit_name = data_selected['resId'], data_selected['name']\n",
    "preservation_id, preservation_name = data_selected['defaultPreservationPolicy']['resId'],data_selected['defaultPreservationPolicy']['name'] \n",
    "submission_id, submission_name = data_selected['defaultSubmissionPolicy']['resId'],data_selected['defaultSubmissionPolicy']['name']\n",
    "\n",
    "print(f'\\nOrganizational Unit \"{orgunit_name}\" with defaults submission policy \"{submission_name}\" and preservation policy \"{preservation_name}\" chosen')"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "c7a8be94-3e10-4912-987e-adaece8f31af",
   "metadata": {},
   "source": [
    "### Finding the other IDs\n",
    "\n",
    "Except for the contributors, similar methods (`admin_licenses_get`,`admin_languages_get`) can be used to retrieve the others identifiers, using the same configuration and API objects. To ease the extraction of data, we have defined below a function that produces a readable shortlist according to the object we want to list and the parameters we want to retrieve from those. The identifiers you are prompted to select will be saved to the variables at the bottom of the cell:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "e8c41bb3-4800-40cb-9b88-e9c7f5c056cf",
   "metadata": {},
   "outputs": [],
   "source": [
    "from typing import Dict\n",
    "\n",
    "def ids_and_vals_of_dlcm_resource(conf: dlcmapi_client.Configuration, \n",
    "                                 ApiType: type,\n",
    "                                 resource_get_func_name: str, \n",
    "                                 val: str,\n",
    "                                 amount_of_results: int = 20,\n",
    "                                 ) -> Dict[str,str]:\n",
    "    with dlcmapi_client.ApiClient(conf) as api_client:\n",
    "        curr_api = ApiType(api_client)\n",
    "        resource_get_function = getattr(curr_api, resource_get_func_name)\n",
    "        data = resource_get_function(size=amount_of_results)\n",
    "        return {el['resId']: el[val] for el in data.to_dict()['data']}\n",
    "\n",
    "data_fetched = []\n",
    "for func, val in {\"admin_licenses_get\": \"title\",\n",
    "                  \"admin_languages_get\": \"code\"}.items():\n",
    "    \n",
    "    resource_name = func.replace('admin_', '').replace('_get','')\n",
    "    #pretty printing the correspondance between id and the value to extract from the current resource to be listed\n",
    "    print(\"\\n\\033[1m%s\" % (resource_name + \" list\"))\n",
    "    ids_vals = ids_and_vals_of_dlcm_resource(admin_conf, dlcmapi_client.AdminApi, func, val).items()\n",
    "    for i, (identifier, val) in enumerate(ids_vals):\n",
    "        print('\\033[0m%2.2d'%i, ' -> ', val)\n",
    "\n",
    "    sel = input(f\"Which {resource_name} would you like to attribute to your deposit (enter its index number located on the left of the arrows)?\")\n",
    "    while not sel.isnumeric() or int(sel) < 0 or int(sel) >= len(ids_vals):\n",
    "        sel = input(f'Please enter a positive integer below {len(ids_vals)}')\n",
    "    \n",
    "    data_selected = list(ids_vals)[int(sel)]\n",
    "    print(f'\"{data_selected[1]}\" selected.')\n",
    "    data_fetched.append(data_selected[0])\n",
    "\n",
    "#saving the two identifiers retrieved in dedicated variables for later use.\n",
    "license_id, language_id = data_fetched"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b3940b4d-2276-4757-9630-629ed513a066",
   "metadata": {},
   "source": [
    "### Fetching Contributors IDs\n",
    "\n",
    "A similar process is used to retrieve the identifiers of contributors, except we are not targeting the administration module of DLCM, but its preingestion module. That means we need to instantiate a different configuration whom the preingestion module URL is fed to.\n",
    "\n",
    "Go back to your modules list, and copy paste the preingestion URL **up until the last slash**:\n",
    "\n",
    "\n",
    "![dlcm-modules-list-preing](imgs/dlcm_preingest_modules_list.png \"DLCM modules with preingest highlighted\")\n",
    "Create a new configuration object using this URL and reusing your access token:\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "5afd8280-ace5-465d-ae64-ee6d2a97772f",
   "metadata": {},
   "outputs": [],
   "source": [
    "preingest_conf = dlcmapi_client.Configuration('https://sandbox.dlcm.ch/ingestion')\n",
    "preingest_conf.access_token = my_access_token"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "6cfe8a68-1295-46f0-8cd7-420eb0b7e5fb",
   "metadata": {},
   "source": [
    "We can reuse the previously defined `ids_and_vals_of_dlcm_resource` function giving it the `PreingestApi` API type, the function `preingest_contributors_get`, and targeting the `fullName` attribute of the contributor object."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "b15c9b39-2eb4-4fb7-9480-d0caa41f633b",
   "metadata": {},
   "outputs": [],
   "source": [
    "contributor_data = ids_and_vals_of_dlcm_resource(preingest_conf, dlcmapi_client.PreingestApi, 'preingest_contributors_get', 'fullName', amount_of_results=2000)\n",
    "list(contributor_data.items())[:30]"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "7dc1bcf4-f006-441b-bfb2-7a57d68494ec",
   "metadata": {},
   "source": [
    "Note the function's parameter `amount_of_results` normally is not set, which attributes the amount of results per request to its default of 20. As we want to search through all the contributors, we manually set this number to the maximum amount of results in a single request, which is 2000 (but displayed only 30 to avoid overwhelming the notebook's output).\n",
    "\n",
    "Similarly to previous examples, below is a routine that helps you search through the list of contributors fetched from the previous data and allows you to select the ones you want to save:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "4ea301a0-8378-418a-8cc3-422280b420b6",
   "metadata": {},
   "outputs": [],
   "source": [
    "number_of_contributors = int(input(\"How many contributors you'd like to add?\"))\n",
    "while number_of_contributors < 1:\n",
    "    number_of_contributors = int(input(\"Enter a positive number above 1:\"))\n",
    "\n",
    "contributors_list = []\n",
    "\n",
    "for k in range(number_of_contributors):\n",
    "    candidates = set()\n",
    "    while(len(candidates)<1):\n",
    "        cont_name = input(f\"\\nEnter the name or parts of the name of the contributor #{k+1} you would like to add:\")\n",
    "        names = [v.lower() for v in cont_name.split(' ')]\n",
    "        for cid, cname in list(contributor_data.items()):\n",
    "            cnames = [c.lower() for c in cname.split(',')]\n",
    "            for n in names:\n",
    "                for cn in cnames:\n",
    "                    if n in cn:\n",
    "                        candidates.add((cid, cname))\n",
    "\n",
    "        candidates_list = list(candidates)\n",
    "        if len(candidates_list) < 1:\n",
    "            print(f'\\nCould not find a contributor whose name resemble \"{cont_name}\", try again with a different input')\n",
    "        elif len(candidates_list) == 1:\n",
    "            print(f'\\nFound only \"{candidates_list[0][1]}\" matching your input, saving them as contributor #{k+1}')\n",
    "            contributors_list.append(candidates_list[0])\n",
    "        else:\n",
    "            print()\n",
    "            for i, (_, cand_name) in enumerate(candidates_list):\n",
    "                print('\\033[0m%2.2d'%i, ' -> ', cand_name)\n",
    "            sel = input(f'\\nWhich person would you like to set as contributor #{k+1} to your deposit? (enter its select. number left of the arrow)')\n",
    "            contributors_list.append(candidates_list[int(sel)])\n",
    "\n",
    "print('\\nContributors chosen:')\n",
    "for i, (_, contributor_name) in enumerate(contributors_list):\n",
    "    print(f'Contributor #{i+1}: {contributor_name}')"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "f2a3b294-3b53-4d62-ad7a-0f747dffbd7e",
   "metadata": {},
   "source": [
    "### Create and fill in an _offline_ Deposit\n",
    "A deposit object can be instantiated using the library this way:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "411493d1-f867-46ef-b655-9753fd939466",
   "metadata": {},
   "outputs": [],
   "source": [
    "import dlcmapi_client\n",
    "deposit = dlcmapi_client.Deposit()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "8063381c-2568-4ebb-98db-95c0f1729ae7",
   "metadata": {},
   "source": [
    "This deposit object for now is an offline abstraction of an online DLCM deposit, it has several fields that can be set, such as its organizational unit, its title, its description and so forth. To see all the available fields of a Deposit object you can interact with, the following command can be used:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "7352fe6c-fb42-4731-b442-be7cece7ab62",
   "metadata": {},
   "outputs": [],
   "source": [
    "[attr for attr in dir(deposit) if not attr.startswith('_')] #removing some uninteresting default methods that starts with \"_\""
   ]
  },
  {
   "cell_type": "markdown",
   "id": "61acd91b-4d7e-4f1a-b035-69e8e0dd96e2",
   "metadata": {},
   "source": [
    "After choosing the right identifiers from previous listings, run the cell below to fill in the required values we saved onto the deposit object's fields (you can also directly add code in the cell for any other fields available in the deposit you'd like to be set). Likewise with the fetching of identifiers, setting the contributors in a deposit requires a different process explained later in the notebook."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "931582e1-7059-4dbf-9810-384c220f162f",
   "metadata": {},
   "outputs": [],
   "source": [
    "#can be set to whatever title and description fits the kind of deposit you'd like to do\n",
    "deposit.title = input(\"Enter the title of your deposit:\")\n",
    "deposit.description = input(\"Enter the description of your deposit:\")\n",
    "\n",
    "#madatory IDs to be set\n",
    "deposit.organizational_unit_id = orgunit_id\n",
    "deposit.submission_policy_id = submission_id\n",
    "deposit.preservation_policy_id = preservation_id\n",
    "\n",
    "#optional IDs\n",
    "deposit.license_id = license_id\n",
    "deposit.language_id = language_id\n",
    "\n",
    "#checking the values have been correctly set in the object:\n",
    "print(deposit)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "0ad2071d-9ac2-43c9-9511-847ccafa0d08",
   "metadata": {},
   "source": [
    "### Post a deposit online\n",
    "\n",
    "To post the deposit, we reuse our `PreIngest` configuration to instantiate an `ApiClient`, which in turn is used to correctly instantiate a `PreingestApi` Object. This API object gives us access the the method `preingest_deposits_post` that can receive our deposit object previously prepared and post it to the server:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "893495c9-be7c-4414-9ff3-01377027e1b5",
   "metadata": {},
   "outputs": [],
   "source": [
    "with dlcmapi_client.ApiClient(preingest_conf) as api_client:\n",
    "    try:            \n",
    "        preingest_api = dlcmapi_client.PreingestApi(api_client)\n",
    "        preingest_api.api_client.client_side_validation = False\n",
    "        res = preingest_api.preingest_deposits_post(deposit=deposit)\n",
    "        deposit_id = res.to_dict()['res_id']\n",
    "    except dlcmapi_client.ApiException as e:\n",
    "        print(\"Exception when calling method: %s\\n\" % e)\n",
    "        \n",
    "deposit_id"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "64b2adb4-495c-4fc0-ad25-d733bef39dbb",
   "metadata": {},
   "source": [
    "Notice how we retrieved the return value of `preingest_deposits_post`, in order to retrieve the `res_id` value it holds. This value is the unique identifier the server attributed to the deposit we just posted. It is going to be needed in order to add contributors to our deposit, the last mandatory step to complete the deposit.\n",
    "\n",
    "### Adding contributors to a preexisting deposit\n",
    "\n",
    "Adding contributors to a deposit is a similar process to the post of a deposit, except the function `preingest_deposits_contributors_post` is used, and it expects the deposit's ID and a list of contributors IDs as parameters. Run the cell below to post the contributors that were selected in the \"Fetching Contributors ID\" section of the notebook:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "25894453-1b87-4691-9c77-2c0411bde379",
   "metadata": {},
   "outputs": [],
   "source": [
    "contributors_id = [i for i,v in contributors_list]\n",
    "with dlcmapi_client.ApiClient(preingest_conf) as api_client:\n",
    "    try:            \n",
    "        preingest_api = dlcmapi_client.PreingestApi(api_client)\n",
    "        preingest_api.api_client.client_side_validation = False\n",
    "        preingest_api.preingest_deposits_contributors_post(deposit_id=deposit_id, contributors_list=contributors_id)\n",
    "    except dlcmapi_client.ApiException as e:\n",
    "        print(\"Exception when calling method: %s\\n\" % e)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "d0b5e84d-9059-4910-8d18-8de0229a126b",
   "metadata": {},
   "source": [
    "You can go back to your preservation space on your DLCM's instance to check the deposit has correctly been created and set up."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "f7a1fa88-3c6f-4009-aeee-26642a2b49a9",
   "metadata": {},
   "source": [
    "### Reserving a DOI \n",
    "\n",
    "Finally if you'd like to reserve a DOI for your newly created deposit, it is possible through the custom function defined below, which will ask DLCM's backend to reserve a doi indicated by the `deposit_id` and return it:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "5480bc3e-216d-47ca-8f96-25a9f592ceb8",
   "metadata": {},
   "outputs": [],
   "source": [
    "import requests\n",
    "import json\n",
    "\n",
    "def reserve_doi_for_deposit(preingest_conf: dlcmapi_client.Configuration, deposit_id: str)->str:\n",
    "    '''\n",
    "    Reserve and return a DOI for the deposit indicated by id \"deposit_id\" within the preingestion module configured in the parmater \"preingest_conf\"\n",
    "    '''\n",
    "    target_url = f'{preingest_conf.host}/preingest/deposits/{deposit_id}/reserve-doi'\n",
    "    headers = {\"Authorization\": f\"Bearer {preingest_conf.access_token}\"}\n",
    "    res = requests.post(target_url, headers=headers)\n",
    "    if res.status_code != 200:\n",
    "        raise Exception(f'Could not reserve DOI for deposits \"{deposit_id}\" in module \"{preingest_conf}\", received error code \"{res.status_code}\"')\n",
    "    else:\n",
    "        return json.loads(res.content.decode('utf-8'))['doi']\n",
    "    \n",
    "doi = reserve_doi_for_deposit(preingest_conf, deposit_id)\n",
    "doi"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "7036ceb9-2b67-4dbc-9114-58600eb5f206",
   "metadata": {},
   "source": [
    "## Prepare a CSV Template for batch deposits\n",
    "All the previous steps can easily be done manually through the web UI interface of your DLCM instance. Using the API package truly gets interesting and time beneficial once it is used to create a large number of deposits. To do so, using all the ids we extracted from the previous cell, we are going to create a \"template\" CSV file holding those ids, which you can then manually edit to specify specific title and description for each sub deposit. In a later cell, we are going to read that CSV to parse deposits data and create them online."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "b497cd72-2a60-4cb3-b2c5-3e5093185f20",
   "metadata": {},
   "outputs": [],
   "source": [
    "import pandas as pd\n",
    "\n",
    "if not contributors_id:\n",
    "    contributors_id = [i for i,v in contributors_list]\n",
    "\n",
    "nmb_entries = 3\n",
    "data = {\n",
    "    'Title': ['']*nmb_entries,\n",
    "    'Description': ['']*nmb_entries,\n",
    "    'OrgUnitId': [orgunit_id]*nmb_entries,\n",
    "    'PreservationPolicyId': [preservation_id]*nmb_entries,\n",
    "    'SubmisionPolicyId': [submission_id]*nmb_entries,\n",
    "    'LicenseId': [license_id]*nmb_entries,\n",
    "    'LanguageId': [language_id]*nmb_entries,\n",
    "    'ContributorsIds': [','.join(contributors_id)]*nmb_entries,\n",
    "}\n",
    "df = pd.DataFrame(data)\n",
    "df.to_csv('MyDLCMTemplateForBatchUpload.csv', index=False)\n",
    "df"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "9791aa22-f263-42c8-9ade-37de1865a0cb",
   "metadata": {},
   "source": [
    "## Make multiple deposits on DLCM from data in a CSV\n",
    "\n",
    "Customize the CSV file we created in the cell above so that each deposit has a title and a description. Feel free to reuse any of the of the several snippets of the code from previous cells if you need to fetch other types of identifiers or information you'd like to fill in the CSV. Then we can simply load the CSV with pandas (common python library for data treatment) iterate over each row, each time creating a brand new offline deposit, and then posting it online:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "629ed8f7-49b3-4d47-b970-4b73e30db4b3",
   "metadata": {},
   "outputs": [],
   "source": [
    "for idx, row in pd.read_csv('MyDLCMTemplateForBatchUpload2.csv', sep=';').iterrows():\n",
    "    #another way to initialize data in the Deposit, pass them to the Deposit constructor directly!\n",
    "    curr_deposit = dlcmapi_client.Deposit(\n",
    "        title = row['Title'],\n",
    "        description = row['Description'],\n",
    "        organizational_unit_id = row['OrgUnitId'],\n",
    "        submission_policy_id = row['SubmisionPolicyId'],\n",
    "        preservation_policy_id = row['PreservationPolicyId'],\n",
    "        license_id = row['LicenseId'],\n",
    "        language_id = row['LanguageId'],\n",
    "    )\n",
    "    #Posting the deposit and the contributors using the same api client and preingest api to save some lignes of codes\n",
    "    with dlcmapi_client.ApiClient(preingest_conf) as api_client:\n",
    "        try:            \n",
    "            preingest_api = dlcmapi_client.PreingestApi(api_client)\n",
    "            preingest_api.api_client.client_side_validation = False\n",
    "            res = preingest_api.preingest_deposits_post(deposit=curr_deposit)\n",
    "            deposit_id = res.to_dict()['res_id']\n",
    "            preingest_api.preingest_deposits_contributors_post(deposit_id=deposit_id, contributors_list=row['ContributorsIds'].split(','))\n",
    "            deposit_url = f'{preingest_conf.host.replace(\"ingestion\", \"\")}deposit/{row[\"OrgUnitId\"]}/detail/{deposit_id}'\n",
    "            print(f'Deposit #{idx} \"({row[\"Title\"]})\" successfully posted online, you can consult it at {deposit_url}')\n",
    "        except dlcmapi_client.ApiException as e:\n",
    "            print(\"Exception when trying to post deposit #%d while calling method: %s\\n\" % (idx, e))\n",
    "        "
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "dlcm_nb",
   "language": "python",
   "name": "dlcm_nb"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.6"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
